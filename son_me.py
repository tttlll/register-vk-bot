import json
sex={
    'one_time': False,
    'buttons': [[{
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '1'}),
            'label': 'Мужской',
        },
        'color': 'positive'
    },
    {
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '2'}),
            'label': 'Женский',
        },
        'color': 'positive'
    }
    ]]
}
sex=json.dumps(sex, ensure_ascii=False).encode("utf-8")

smoke={
    'one_time': False,
    'buttons': [[{
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '1'}),
            'label': 'Положительно',
        },
        'color': 'positive'
    },
    {
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '2'}),
            'label': 'Нейтрально',
        },
        'color': 'default'
    },
        {
            'action': {
                'type': 'text',
                'payload': json.dumps({'buttons': '2'}),
                'label': 'Отрицательно',
            },
            'color': 'negative'
        }
    ]]
}
smoke=json.dumps(smoke, ensure_ascii=False).encode("utf-8")

alco={
    'one_time': True,
    'buttons': [[{
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '1'}),
            'label': 'Положительно',
        },
        'color': 'positive'
    },
    {
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '2'}),
            'label': 'Нейтрально',
        },
        'color': 'default'
    },
        {
            'action': {
                'type': 'text',
                'payload': json.dumps({'buttons': '2'}),
                'label': 'Отрицательно',
            },
            'color': 'negative'
        }
    ]]
}
alco=json.dumps(alco, ensure_ascii=False).encode("utf-8")

relations={
    'one_time': False,
    'buttons': [[{
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '1'}),
            'label': 'Да',
        },
        'color': 'positive'
    },
    {
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '2'}),
            'label': 'Нет',
        },
        'color': 'negative'
    }
    ]]
}
relations=json.dumps(relations, ensure_ascii=False).encode("utf-8")

register={
    'one_time': False,
    'buttons': [[{
        'action': {
            'type': 'text',
            'payload': json.dumps({'buttons': '1'}),
            'label': 'Зарегистрироваться',
        },
        'color': 'positive'
    }
    ]]
}
register=json.dumps(register, ensure_ascii=False).encode("utf-8")

tabl={
    '2':"sex",
    '3':"relations",
    '4':"smoke",
    '5':"alco",
    '6':"city",
    '7':"age",
    '8':"hobby"
    }
messag={
    '2':"Введите Ваш пол",
    '3':"В отношениях ли вы?",
    '4':"Ваше отношение к курению?",
    '5':"Ваше отношение к алкоголю",
    '6':"Введите ваш город",
    '7':"Введите ваш возраст",
    '8':"Введите ваше любимое дело",
    '9':"🎉 Регистрация успешно завершена! 🎉"

}





def sex_r():
    return sex

def smoke_r():
    return smoke

def alco_r():
    return alco

def relations_r():
    return relations

def tabl_r():
    return tabl

def register_r():
    return register
def message_r():
    return messag


all = {
    '2': sex_r(),
    '3': relations_r(),
    '4':smoke_r(),
    '5':alco_r()
}

def all_r():
    return all
