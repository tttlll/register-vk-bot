# Register-vk-bot
Chat-bot using VK-API. It creates chat-bot using registration system. Bot has 5 button blocks.
### It has 7 Characteristics:
*  Sex
*  Age
*  Attitude to smoke
*  Attitude to alcohol
*  Relations
*  City
*  Hobby

First five have button, last two - No.
# Authorization

In order to invoke method, you need to get **token**. 
Link https://vk.com/club123?act=tokens where 'club123' is your group.

##### You need to set token in "vk_module.py".
`token=''`

##### Also you need to set group id in "vk_module.py"
`group_id=`

# Dependencies
##### Download libs vk,requests.

`pip3 install vk`

`pip3 install requests`

# Admin commands
##### If you want to become admin, add your id in list:
`admins=[123,321,111]`


**/help** - send all commands.

**/reinstall** - reinstall database.

**/showbase** - show database.

# Button properties

**You can change button properties. All is stored in "son_me.py".**
